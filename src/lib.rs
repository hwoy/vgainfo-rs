//! ## vgainfo-rs
//! VGA(0xB8000) info in freestanding Rust.
//!
//! ## Example
//! ```rust
//! #![no_std]
//! extern crate vgainfo_rs;
//! use vgainfo_rs::*;
//!
//! fn reset_screen() {
//!     let buffer_ptr = LENGTH as *mut VgaCharType;
//!     let iter = (0..LENGTH).map(|i| unsafe { buffer_ptr.add(i) });
//!
//!     for ptr in iter {
//!         let value = unsafe { ptr.read_volatile() };
//!         unsafe { ptr.write_volatile(value & 0xff00) };
//!     }
//! }
//! ```

#![no_std]

/// Address of video buffer = 0xB8000
pub const VGA_ADDR: usize = 0xb8000;

/// Column of video buffer = 80
pub const WIDTH: usize = 80;

/// Row of video buffer = 25
pub const HIGH: usize = 25;

/// Size of video buffer = 80 *25
pub const LENGTH: usize = WIDTH * HIGH;

/// Type of video buffer element
pub type VgaCharType = u16;
