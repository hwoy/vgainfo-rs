# vgainfo-rs

VGA(0xB8000) info in freestanding Rust.

## How use

```sh

cargo add vgainfo-rs

```

## Example

```rust
#![no_std]
extern crate vgainfo_rs;
use vgainfo_rs::*;

fn reset_screen() {
    let buffer_ptr = LENGTH as *mut VgaCharType;
    let iter = (0..LENGTH).map(|i| unsafe { buffer_ptr.add(i) });

    for ptr in iter {
        let value = unsafe { ptr.read_volatile() };
        unsafe { ptr.write_volatile(value & 0xff00) };
    }
}

```

## Contact me

- Web: <https://github.com/hwoy>
- Email: <mailto:bosskillerz@gmail.com>
- Facebook: <https://www.facebook.com/watt.duean>